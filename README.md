# ŠkolaOnline CLI

Collection of scripts for interacting with ŠkolaOnline API.  
API reverse engineering - https://bestofphp.com/repo/crpmax-skolaonline-app-api

## Scripts:

### soschedule
Script for retrieving weekly schedule  
Usage: `soschedule [username] {+1|-1}`

### sogrades
Script for retrieving grades  
Usage: `sogrades [username]`

### soinfo
Script for retrieving information about a user  
Usage: `soinfo [username]`

